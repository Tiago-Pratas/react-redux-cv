import React from 'react';
import { Header, Experience, Education } from './components/index';
import { input } from './filler.js';
import './tailwind.output.css'
import './App.scss';

class App extends React.Component {

  state = input;

  addExperience = (job) => {
    this.setState({
      experience: [...this.state.experience, job]
    })
  };

  render() {

    return (
      <div className="app p-4 md:p-10 bg-white">
        <Header data={this.state}/>
        <div class="md:flex mt-20 text-center ml-10 mr-10 md:ml-36 md:mr-36">
        <Education data={this.state}/>
        <Experience data={this.state} addExperience={this.addExperience}/>
        </div>
      </div>
    );

  }
}

export default App;
