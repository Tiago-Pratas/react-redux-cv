const showTime = () => {
    let date = new Date().toLocaleDateString();
    let time = new Date().toLocaleTimeString();
    return (date + ' ' + time);
};

const input = {
    name: 'Tiago',
    surnames: 'Azevedo Pratas',
    job: 'Developer of Stuff',
    about: ` I am enthusiastic about programming and learning how things work. When I was younger I used to take my toys apart and put them back together just to see how they worked. More often than not I would only end up with a broken toy and a stern lecture from my parents on how I should not mess with something if it worked.`,
    about2:`I studied Fine Art, where I developed my aesthetic sensibility, and a sense of how things should look to functioning human beings.
    When I am not programming I’m either listening to Death Metal, playing retro video games or pronoucing potatoes like Sam Gamgee. "Po-tay-toes! Boil em, mash em, stick em in a stew. Lovely big golden chips with a nice piece of fried fish."`,
    
    links: {
        email: 'tiago-pratas@gmail.com',
        linkedin: 'www.linkedin.com',
        twitter: 'www.twitter.com',
        dev: 'www.dev.to/Tiago--Pratas',
        git: 'www.github.com',
    },
    experience: [
    {
        position: 'EFL teacher',
        company: 'Berlitz corp',
        started: 2015,
        ended: 2018,
    },
    {
        position: 'EFL teacher',
        company: 'Vaughan Sys',
        started: 2015,
        ended: 2017,
    },
    {
        position: 'Galp Ready - project assistant',
        company: 'Servicios Prepago Integrales',
        started: 2018,
        ended: setInterval(showTime, 1000),
    },
    ],
    education: [
        {
        institution: 'Escola António Arroio',
        qualification: 'Graphic Design',
        img: "https://cdn0.iconfinder.com/data/icons/graphic-design-solid-1/88/graphic_design_tool_mouse_pencil_pen_ruler-512.png",
        completion: 2006
        },
        {
        institution: 'Portsmouth University',
        qualification: 'BA Fine Arts',
        img: "https://cdn4.iconfinder.com/data/icons/essential-app-1/16/education-degree-course-university-college-512.png",
        completion: 2010
        },
        {
        institution: 'Upgrade Hub',
        qualification: 'Fullstack Web dev',
        img: "https://cdn4.iconfinder.com/data/icons/documents-42/512/document_file_paper_page-32-512.png",
        completion: 2021
        },
        {
        institution: 'Harvardx',
        qualification: 'Intro to Computer Science',
        img: "https://cdn2.iconfinder.com/data/icons/solid-apps-and-programming/32/Applications_and_Programming_application_coding_terminal-512.png",
        completion: 2021
        }]
}

export { input, showTime };