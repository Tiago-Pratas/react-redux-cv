import React, { Component } from 'react';
import { Form } from './Form/form';
import '../../tailwind.output.css';

class Experience extends Component {

    render () {

        const { experience } = this.props.data;

        const list = experience.map(el => (
            <div class="relative text-left">
                
                <div class="flex items-center relative">
                    <div class="hidden md:block w-20">
                        <div class="font-bold italic">
                            {el.started}
                        </div>
                        <div class="md:flex space-x-1 text-xs">
                            <div>{el.started}</div>
                            <div></div>
                            <div>{el.ended}021</div>
                        </div>                        
                    </div>
                    
                    <div class="border-r-2 border-black absolute h-full left-1 md:left-20 top-2 z-10">
                        <i class="fas fa-circle -top-1 -ml-2 absolute"></i>
                    </div>

                    <div class="ml-10">
                        <div class="font-bold">{el.position}</div>
                        <div class="italic md:mb-4">{el.company}</div>
                        <div class="mb-4 mt-2 md:hidden">
                            <div class="text-xs">Abril - Junio</div>
                        </div>
                        <div class="mb-10">Fusce auctor gravida dui, ut tristique nisi aliquam quis. Maecenas id ligula ac dui mollis tempor. Sed vitae ex eros. Proin nisl felis, consectetur sed elit sed, vestibulum ultrices nibh.</div>
                    </div>
                </div>
                    </div>
                

        ))

        

        return (
            <div class="md:w-2/3">
            <h1 class="font-bold italic mb-10">Experience</h1>
                {list}
                <div>
                <Form addExperience={this.props.addExperience}/>
                </div>
            </div>
        )
        
    }
};

export { Experience };