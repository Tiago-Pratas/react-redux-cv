import React, { Component } from 'react';
import '../../../tailwind.output.css';


class Form extends Component {

    state = {
        position: '',
        company: '',
        started: '',
        ended: '',
        isOpen: false,
    }

    //TODO: add form validation method and change form classes

    onFormOpening = (ev) => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    onInputChange = (ev) => {
        const { name, value } = ev.target;

        this.setState({
            [name]: value,
        })
    }

    onSubmit = (ev) => {
        ev.preventDefault();
        this.props.addExperience(this.state);
        this.setState(this.state);
    };

    

    render () {

        return (
        <div>
            <div class='flex max-w-sm w-full bg-white rounded-lg overflow-hidden mx-auto'>
                <div class='w-2 bg-gray-800'></div>

                    <div class='flex items-center px-2 py-3'>
                        <form class="w-full max-w-sm" onSubmit={this.onSubmit}>
                            <div class="md:flex md:items-center mb-6">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-500 font-bold md:text-right 
                                        mb-1 md:mb-0 pr-4" 
                                        for="position">
                                        Position
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input class="bg-gray-100 appearance-none border-2 border-gray-200 rounded w-full 
                                        py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white 
                                        focus:border-blue-300" 
                                        id="position"
                                        name="position"
                                        type="text" 
                                        value={this.state.position}
                                        onChange={this.onInputChange}
                                        placeholder="Position"/>
                                </div>
                            </div>

                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold md:text-right 
                                    mb-1 md:mb-0 pr-4" 
                                    for="company">
                                    Company
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input class="bg-gray-100 appearance-none border-2 border-gray-200 rounded 
                                    w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none 
                                    focus:bg-white focus:border-blue-300" 
                                    id="company"
                                    value={this.state.company}
                                    onChange={this.onInputChange} 
                                    type="text" 
                                    placeholder="Name of the Company"/>
                            </div>
                        </div>

                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold 
                                    md:text-right mb-1 md:mb-0 pr-4"
                                    for="Started">
                                    Started
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input class="bg-gray-100 appearance-none border-2 border-gray-200 
                                    rounded w-full py-2 px-4 text-gray-700 leading-tight 
                                    focus:outline-none focus:bg-white focus:border-blue-300" 
                                    id="inline-password" 
                                    type="number" 
                                    max="2021" min="1900" 
                                    value={this.state.started}
                                    onChange={this.onInputChange} 
                                    placeholder="Start year"/>
                            </div>
                        </div>

                        <div class="md:flex md:items-center mb-6">
                            <div class="md:w-1/3">
                                <label class="block text-gray-500 font-bold 
                                    md:text-right mb-1 md:mb-0 pr-4" 
                                    for="Ended">
                                    Ended
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input class="bg-gray-100 appearance-none border-2 border-gray-200 
                                    rounded w-full py-2 px-4 text-gray-700 leading-tight 
                                    focus:outline-none focus:bg-white focus:border-blue-300" 
                                    id="inline-password" 
                                    type="number" 
                                    max="2021" min="1900"
                                    value={this.state.ended}
                                    onChange={this.onInputChange}   
                                    placeholder="End year"/>
                            </div>
                        </div>
                        
                            <div class="md:flex md:items-center">
                            <div class="md:w-1/3"></div>
                                <div class="md:w-2/3">
                                    <button class="shadow bg-blue-400 hover:bg-blue-300 
                                        focus:shadow-outline focus:outline-none 
                                        text-white font-bold py-2 px-4 rounded" 
                                        onClick={this.onFormOpening}
                                        type="submit">
                                    Add Job
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        )
    }
}

export { Form };