import React, { Component } from 'react';
import './education.scss';
import '../../tailwind.output.css';

class Education extends Component {

    render () {
        const { education } = this.props.data;

        const list = education.map(el => (
            
                    <div class="relative text-left">
                    
                    <div class="flex items-center relative pb-5 justify-start">
                        
                        <div class="border-r-2 border-black absolute h-full top-2 z-10 ">
                            <i class="fas fa-circle -top-1 -ml-2 absolute"></i>
                            <div class="absolute section -ml-8 font-bold italic text-gray-400 ">{el.completion}</div>
                        </div>
    
                        <div class="ml-6 pt-5">
                            <div class="flex mb-4 space-x-16">
                                <div>
                                    {el.institution}
                                </div>
                                <div>
                                    {el.qualification}
                                </div>
                            </div>
                            <div>
                                    <img class="w-10" src={el.img} alt={el.institution}/>
                            </div>
                        </div>
                    </div>
                </div>
            
        ))

        return (
                <div class="md:mr-20 md:w-1/3">
                    <h1 class="font-bold italic mb-10">Education</h1>
                    { list }
                </div>
)
    }
}

export { Education };

