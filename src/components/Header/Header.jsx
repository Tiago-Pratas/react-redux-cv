import React, { Component } from 'react';
import '../../tailwind.output.css';

class Header extends Component  {
    render () {
        return (
            <React.Fragment>
                <div class="flex justify-center text-xl font-bold tracking-widest text-gray-300">
        <h1>Developer of Stuff</h1>
    </div>

    <div class="grid col-span-1 md:flex items-center mt-10 justify-center">
        <div class="mr-14">
        <img class="md:w-40" src="https://i.ibb.co/tsGYK0C/avataaars.png" alt=""/>
        </div>
        <div class="md:mr-4">
            <img class="md:w-80" src="https://i.ibb.co/sKMMzsz/facebook-profile-image.png" alt="Logo"/>
        </div>
        <div class="md:border-l-2 pl-4 p-2 col-span-2 text-justify md:w-1/2 mt-10 md:mt-0">
            <p>
                {this.props.data.about}
            </p>
            <p class="mt-4">
                {this.props.data.about2}
            </p>
        </div>
    </div>

    <div class="grid col-span-1 md:flex items-center justify-center mt-20">
        <div>
            <div class="md:flex items-center mb-4">
                <div class="flex items-center md:mr-8 mb-4 md:mb-0">
                    <i class="fas fa-envelope fa-2x mr-2 opacity-70"></i>
                    <p>{this.props.data.links.email}</p>
                </div>

                <div class="flex items-center">
                    <i class="fab fa-github fa-2x mr-2 opacity-70"></i>
                    <p>{this.props.data.links.git}</p>
                </div>
            </div>
            <div class="md:flex items-center mb-4">
                <div class="flex items-center md:mr-8 mb-4 md:mb-0">
                    <i class="fab fa-dev fa-2x mr-2 opacity-70"></i>
                    <p>{this.props.data.links.dev}</p>
                </div>

                <div class="flex items-center">
                    <i class="fab fa-linkedin fa-2x mr-2 opacity-70"></i>
                    <p>{this.props.data.links.linkedin}</p>
                </div>
            </div>
            
        </div>
        

        <div class="flex items-center justify-center md:ml-12 pt-10 md:pt-0">
            <div class="md:flex">
                <div class="flex items-center mb-4 md:mb-0">
                    <p class="font-bold mr-2 p-2 border rounded-full">ES</p>
                    <p>C1</p>
                </div>

                <div class="flex items-center md:ml-10">
                    <p class="font-bold mr-2 p-2 border rounded-full">EN</p>
                    <p>C2</p>
                </div>

                <div class="flex items-center md:ml-10">
                    <p class="font-bold mr-2 p-2 border rounded-full">PT</p>
                    <p>Native</p>
                </div>
            </div>            
        </div>
    </div>
            </React.Fragment>
            )
    }
}

export { Header }